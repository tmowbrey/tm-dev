/**
 * Created by tmowbrey on 8/23/2016.
 */
({
    handleLocationSelected : function (component, event, helper) {

        // Get location data and delegate to the helper for acquiring information about the location
        var receivedLocation = event.getParam("location");
        helper.getLocationData(component, receivedLocation);

    }
})