/**
 * Created by tmowbrey on 8/24/2016.
 */
({
    getLocationData : function (component, receivedLocation) {

        // Call our apex controller to get information from Nominatim
        var action = component.get("c.getLocationData");
        action.setParams({
            location : receivedLocation
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // Success!! process our data
                this.processLocationDataToBoundingData(response.getReturnValue());
            } else if (state === "ERROR" && response.getError()) {
                // Error :( send a message up to our message component
                var errorMessage = $A.get("e.c:MessageSelected");
                errorMessage.setParams({
                    "state" : state,
                    "message" : response.getError()[0].message
                });
                errorMessage.fire();
            }
        });
        $A.enqueueAction(action);

    },

    processLocationDataToBoundingData : function (locationData) {

        // Convert locationData to a javascript object and process
        var locationObject = JSON.parse(locationData);
        if(locationObject === null || locationObject.length == 0) {
            // Error :( send a message up to our message component
            var errorMessage = $A.get("e.c:MessageSelected");
            errorMessage.setParams({
                "state" : "ERROR",
                "message" : "That does not seem like a known location, please try again."
            });
            errorMessage.fire();
        } else {
            // API returns values in order based on importance. The first value is most important
            // so use that as our location bounding box
            var boundingBox = locationObject[0].boundingbox;

            // Fire out BoundingBoxSelected event with our processed details
            var boundingBoxSelected = $A.get("e.c:BoundingBoxSelected");
            boundingBoxSelected.setParams({
                "boundingBox" : boundingBox
            });
            boundingBoxSelected.fire();
        }

    }
})