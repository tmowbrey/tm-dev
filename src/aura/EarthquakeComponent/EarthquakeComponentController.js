/**
 * Created by tmowbrey on 8/24/2016.
 */
({
    handleBoundingBoxSelected : function (component, event, helper) {

        // Get boudning box data and delegate to the helper for acquiring earthquake data
        var boundingBox = event.getParam("boundingBox");
        helper.getEarthquakeData(component, boundingBox);

    }
})