/**
 * Created by tmowbrey on 8/24/2016.
 */
({
    getEarthquakeData : function (component, boundingBox) {

        // Call our apex controller to get information from Geonames
        var action = component.get("c.getEarthquakeData");
        action.setParams({
            geoLocationStrings : boundingBox
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // Success!! process our data
                this.processEarthquakeData(component, response, boundingBox);
            } else if (state === "ERROR" && response.getError()) {
                // Error :( send a message up to our message component
                var errorMessage = $A.get("e.c:MessageSelected");
                errorMessage.setParams({
                    "state" : state,
                    "message" : response.getError()[0].message
                });
                errorMessage.fire();
            }
        });
        $A.enqueueAction(action);

    },

    processEarthquakeData : function(component, response, boundingBox) {

        // Convert earthquake to a javascript object and process
        var earthquakeData = JSON.parse(response.getReturnValue());
        if(earthquakeData && earthquakeData.earthquakes && earthquakeData.earthquakes.length > 0) {
            // Create markers. This process takes our Geonames data and maps it to
            // something our MarkersSelected Event can understand.
            var markers = [];
            markers = earthquakeData.earthquakes.map(function (item) {
                return {
                    lat: item.lat,
                    lon: item.lng,
                    title1: "Date/Time: " + item.datetime,
                    title2: "Magnitude: " + item.magnitude
                };
            });
            // Fire our MarkersSelected Event with our markers and boundingbox
            var markersSelected = $A.get("e.c:MarkersSelected");
            markersSelected.setParams({
                "markers" : markers,
                "bounds" : boundingBox
            });
            markersSelected.fire();
        } else {
            // We didn't find any earthquake data!! Send a message to our message component
            var errorMessage = $A.get("e.c:MessageSelected");
            errorMessage.setParams({
                "state" : "ERROR",
                "message" : "No earthquake data found for the input location. Please try a different location."
            });
            errorMessage.fire();
        }

    }
})