/**
 * Created by tmowbrey on 8/23/2016.
 */
({
    onDoSearch : function (component, event, helper) {

        // Get the location from the input field and delegate to our helper to fire an event
        var locationParam = component.find("location").get("v.value");
        helper.fireLocationSelected(locationParam);

    }
})