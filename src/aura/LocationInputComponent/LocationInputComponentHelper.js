/**
 * Created by tmowbrey on 8/24/2016.
 */
({
    fireLocationSelected : function (locationParam) {

        // Fire our LocationSelected Event with our location
        var locationEvent = $A.get("e.c:LocationSelected");
        locationEvent.setParams({
            "location" : locationParam
        });
        locationEvent.fire();

        var confirmationMessage = $A.get("e.c:MessageSelected");
        confirmationMessage.setParams({
            "state" : "SUCCESS",
            "message" : "Searching for Earthquakes in that Location!"
        });
        confirmationMessage.fire();

    }
})