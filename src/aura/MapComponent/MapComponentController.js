/**
 * Created by tmowbrey on 8/25/2016.
 */
({
    loadMap : function (component, event, helper) {

        // Initialize the Leaflet map. Timeout is required to ensure the DOM has been fully built
        setTimeout(function() {
            var map = L.map('map', {zoomControl: false})
                .setView([37.784173, -122.401557], 14);
            L.tileLayer(
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                {
                    attribution: 'Tiles © Esri'
                }).addTo(map);
            component.set("v.map", map);
        });

    },

    handleMarkersSelected : function (component, event, helper) {

        // Get markers and delegate to the helper for adding markers to the map
        var markersSelected = event.getParam("markers");
        helper.addMarkers(component, markersSelected);

    }
})