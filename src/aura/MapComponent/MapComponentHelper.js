/**
 * Created by tmowbrey on 8/25/2016.
 */
({
    addMarkers: function(component, markersSelected) {

        // Check if map is initialized, if so, destroy it. This is required due to
        // new changes with LockerService. I am unable to use map.panTo or map.panInsideBounds
        // Due to undefined errors. These error are occurring because of a bug with LockerService.
        // To get around the issue, I am re-creating the map every time a new search occurs
        var map = component.get("v.map");
        if(map) {
            map.remove();
        }

        // Loop through the passed in markers and add them to the map
        var markers = [];
        if (markersSelected && markersSelected.length > 0) {
            for (var i=0; i<markersSelected.length; i+=1) {
                var markerSelected = markersSelected[i];
                if (markerSelected.lat && markerSelected.lon) {
                    var latLng = [markerSelected.lat, markerSelected.lon];
                    var marker = new L.marker(latLng)
                                    .bindPopup("<b>" + markerSelected.title1 + "</b><br>" + markerSelected.title2);
                    markers.push(marker);
                }
            }
            // Create a marker group
            var markerGroup = new L.featureGroup(markers);
            // Determine the boundary that includes all markers
            var bounds = new L.latLngBounds(markerGroup.getBounds());
            // Create the map
            map = L.map('map', {zoomControl: false});
            // Set the view, bounds, and tile necessary to show the markers
            map.setView(new L.latLng(bounds.getCenter()));
            map.fitBounds(bounds);
            L.tileLayer(
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                {
                    attribution: 'Tiles © Esri'
                }).addTo(map);
            // Add markers to the map and push the map to the view
            map.addLayer(markerGroup);
            component.set("v.map", map);
        }

    }
})