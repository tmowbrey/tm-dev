/**
 * Created by tmowbrey on 8/24/2016.
 */
({
    handleMessageSelected : function (component, event, helper) {

        // Obtain the received message and state to determine what type of message to show
        // then delegate the work to our helper
        var state = event.getParam("state");
        var message = event.getParam("message");

        if(state === 'SUCCESS') {
            helper.handleConfirmationMessage(component, message);
        } else if(state === 'ERROR') {
            helper.handleErrorMessage(component, message);
        }

    }
})