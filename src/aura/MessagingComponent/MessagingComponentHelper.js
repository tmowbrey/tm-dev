/**
 * Created by tmowbrey on 8/24/2016.
 */
({
    handleConfirmationMessage : function (component, message) {

        // Set the type of message and delegate to internal help method
        var title = "Confirmation:";
        var severity = "confirm";
        this.addMessage(component, title, severity, message);

    },

    handleErrorMessage : function (component, message) {

        // Set the type of message and delegate to internal help method
        var title = "Error Occurred:";
        var severity = "error";
        this.addMessage(component, title, severity, message);

    },

    addMessage : function (component, title, severity, message) {

        // Dynamically create a message and output component.
        $A.createComponents([
                ["ui:message", {
                    "title": title,
                    "severity": severity
                }],
                ["ui:outputText", {
                    "value": message
                }]
            ],
            function (components) {
                // Set components to the view
                var messageComp = components[0];
                var outputText = components[1];
                // set the body of the ui:message to be the ui:outputText
                messageComp.set("v.body", outputText);
                component.set("v.messages", messageComp);
                // Set a timeout so that we can clear the message after 5000 ms
                window.setTimeout(
                    $A.getCallback(function () {
                        if(component.isValid()){
                            component.set("v.messages", []);
                        }
                    }), 5000
                );
            }
        );

    }
})