/**
 * Created by tmowbrey on 8/27/2016.
 */
({
    doInit : function (component, event, helper) {

        // Call our helper to generate the top 10 earthquakes
        helper.getTop10EarthquakesThisYear(component);

    }
})