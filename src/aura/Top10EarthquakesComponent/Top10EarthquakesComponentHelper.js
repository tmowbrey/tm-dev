/**
 * Created by tmowbrey on 8/27/2016.
 */
({
    getTop10EarthquakesThisYear : function (component) {

        // Set our bounding box for the world so that we can get all earthquakes
        var worldBoundingBox = ["-85", "85", "-180", "180"];
        // Call the apex controller to get earthquakes by the wold bounding box
        var action = component.get("c.getTop10EarthquakeData");
        action.setParams({
            geoLocationStrings : worldBoundingBox
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // Success!! process our data
                this.processEarthquakeData(component, response);
            } else if (state === "ERROR" && response.getError()) {
                // Error :( send a message up to our message component
                var errorMessage = $A.get("e.c:MessageSelected");
                errorMessage.setParams({
                    "state" : state,
                    "message" : response.getError()[0].message
                });
                errorMessage.fire();
            }
        });
        $A.enqueueAction(action);

    },

    processEarthquakeData : function (component, response) {

        // Create javascript object from our response
        var earthquakeData = JSON.parse(response.getReturnValue());
        if(earthquakeData && earthquakeData.earthquakes && earthquakeData.earthquakes.length > 0) {
            // Filter our response to ensure we only have the top 10 earthquakes from this year
            // then push our data to the top 10 earthquake table
            var top10 = earthquakeData.earthquakes.filter(this.isLast12Months).slice(0, 10);
            // Remove time portion for display purposes
            for(var key in top10) {
                top10[key].datetime = top10[key].datetime.split(" ")[0];
            }
            component.set("v.top10Earthquakes", top10);
        }

    },

    isLast12Months : function (earthquake) {

        // Get the earthquake datetime and convert it to a javascript date
        var earthquakeDate = new Date(Date.parse(earthquake.datetime));
        // Get today's date
        var oldestDate = new Date(Date.now());
        // Subtract 12 months from today's date
        oldestDate.setMonth(oldestDate.getMonth()-12);
        // return true/false based on whether the earthquake date is larger/equal to the date 12 months before today
        return earthquakeDate >= oldestDate;

    }
})