/**
 * Created by tmowbrey on 8/23/2016.
 */

public with sharing class BoundingBoxController
{
    @AuraEnabled
    public static String getLocationData(String location)
    {
        if(location == '' || location == NULL)
        {
            AuraHandledException e = new AuraHandledException('You must supply a location.');
            e.setMessage('You must supply a location.');
            throw e;
        }

        String jsonResponse;

        try
        {
            jsonResponse = NominatimService.querySearchJSON(new List<String>{location});
        }
        catch(Exception ex)
        {
            AuraHandledException e = new AuraHandledException(ex.getMessage());
            e.setMessage(ex.getMessage());
            throw e;
        }

        return jsonResponse;
    }
}