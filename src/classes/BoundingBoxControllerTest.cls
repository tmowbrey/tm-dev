/**
 * Created by tmowbrey on 8/27/2016.
 */
@isTest
private class BoundingBoxControllerTest
{
    @isTest
    static void testSuccessfulResponse()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestNominatimJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);

        test.startTest();

        String jsonResponse = BoundingBoxController.getLocationData('California');

        test.stopTest();

        system.assertEquals('[{"place_id":"143927648","licence":"Data © OpenStreetMap contributors, ODbL 1.0. http:\\/\\/www.openstreetmap.org\\/copyright","osm_type":"relation","osm_id":"165475","boundingbox":["32.5295236","42.009499","-124.4820029","-114.1307815"],"lat":"36.7014631","lon":"-118.7559973","display_name":"California, United States of America","class":"boundary","type":"administrative","importance":0.954818372042,"icon":"https:\\/\\/nominatim.openstreetmap.org\\/images\\/mapicons\\/poi_boundary_administrative.p.20.png"}]'
                , jsonResponse
        );

    }

    @isTest
    static void testCalloutException()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestNominatimJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);

        test.startTest();

        Account acc = new Account(Name='Test Account');
        insert acc;

        try
        {
            String jsonResponse = BoundingBoxController.getLocationData('California');
            system.assert(FALSE, 'Exception was expected as we are mixing DML and a callout');
        }
        catch(AuraHandledException ex)
        {
            system.assert(TRUE);
            system.assertEquals('You have uncommitted work pending. Please commit or rollback before calling out', ex.getMessage());
        }

        test.stopTest();
    }

    @isTest
    static void testNoLocationProvidedException()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestNominatimJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);

        test.startTest();

        Account acc = new Account(Name='Test Account');
        insert acc;

        try
        {
            String jsonResponse = BoundingBoxController.getLocationData('');
            system.assert(FALSE, 'Exception was expected as we are passing a blank string');
        }
        catch(AuraHandledException ex)
        {
            system.assert(TRUE);
            system.assertEquals('You must supply a location.', ex.getMessage());
        }

        test.stopTest();
    }
}