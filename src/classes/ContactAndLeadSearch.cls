public class ContactAndLeadSearch 
{
	public static List<List< SObject>> searchContactsAndLeads(String searchParameter)
    {
        List<List< SObject>> myResults = [FIND :searchParameter IN NAME FIELDS RETURNING Contact(FirstName, LastName), Lead(FirstName, LastName)];
        
        return myResults;
    }
}