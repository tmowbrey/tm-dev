/**
 * Created by tmowbrey on 8/24/2016.
 */

public with sharing class EarthquakeController
{
    @AuraEnabled
    public static String getEarthquakeData(List<String> geoLocationStrings)
    {
        if(geoLocationStrings == NULL || geoLocationStrings.size() == 0)
        {
            AuraHandledException e = new AuraHandledException('You must supply north/south/east/west geo location strings.');
            e.setMessage('You must supply north/south/east/west geo location strings.');
            throw e;
        }

        String jsonResponse;

        try
        {
            jsonResponse = GeonamesService.queryEarthquakesByBoundingBox(geoLocationStrings, 50);
        }
        catch(Exception ex)
        {
            AuraHandledException e = new AuraHandledException(ex.getMessage());
            e.setMessage(ex.getMessage());
            throw e;
        }

        return jsonResponse;
    }
}