/**
 * Created by tmowbrey on 8/24/2016.
 */

public with sharing class GeonamesService
{
    public static String queryEarthquakesByBoundingBox(List<String> geoLocationStrings, Integer rowLimit)
    {
        return service().queryEarthquakesByBoundingBox(geoLocationStrings, rowLimit);
    }

    private static GeonamesServiceImpl service()
    {
        return new GeonamesServiceImpl();
    }
}