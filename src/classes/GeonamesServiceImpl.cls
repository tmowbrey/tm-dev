/**
 * Created by tmowbrey on 8/24/2016.
 */

public class GeonamesServiceImpl
{
    public String queryEarthquakesByBoundingBox(List<String> geoLocationStrings, Integer rowLimit)
    {
        /*
        *  To meet the requirements of this assigment, I am not bulkifying this process.
        *  Architecture and descision is the same as the NominatimService
        * */

        String responseBody = '';

        if(geoLocationStrings.size() == 4)
        {
            EarthquakeAppSettings__c appSettings = EarthquakeAppSettings__c.getValues('default');
            if(appSettings == NULL || appSettings.Geonames_Username__c == NULL)
                throw new ApplicationSettingException('The EarthquakeApp settings have not been set. Please update the custom setting and try again.');

            // Get Single Location
            String south = geoLocationStrings[0];
            String north = geoLocationStrings[1];
            String west = geoLocationStrings[2];
            String east = geoLocationStrings[3];

            // Construct HTTP request
            Http http = new Http();
            HttpRequest req = new HttpRequest();

            // Set Header Details
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');

            // Set Method and Endpoint
            req.setMethod('GET');
            req.setEndpoint('http://api.geonames.org/earthquakesJSON?username='
                    + appSettings.Geonames_Username__c
                    +'&south='+south+'&north='+north+'&west='+west+'&east='+east+'&maxRows='+rowLimit);

            try
            {
                HttpResponse res = http.send(req);
                responseBody = res.getBody();
            }
            catch(System.CalloutException e)
            {
                throw e;
            }
        }

        return  responseBody;
    }
}