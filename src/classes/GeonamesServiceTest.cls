/**
 * Created by tmowbrey on 8/27/2016.
 */
@isTest
private class GeonamesServiceTest
{
    @isTest
    static void testSuccessfulResponse()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestGeonamesJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);

        EarthquakeAppSettings__c settings = new EarthquakeAppSettings__c(Name='default', Geonames_Username__c='testUser');
        insert settings;

        test.startTest();

        String jsonResponse = GeonamesService.queryEarthquakesByBoundingBox(
                new List<String>{'-85','85','-180','180'}, 10
        );

        test.stopTest();

        system.assertEquals('{"earthquakes":[{"datetime":"2016-08-27 04:54:10","depth":66.12,"lng":-26.9039,"src":"us","eqid":"us20006ugf","magnitude":5.4,"lat":-56.2823},{"datetime":"2016-08-27 15:27:50","depth":6.8,"lng":-158.362,"src":"us","eqid":"us20006ukd","magnitude":5,"lat":55.7872},{"datetime":"2016-08-27 02:41:20","depth":10,"lng":57.8638,"src":"us","eqid":"us20006ufv","magnitude":4.9,"lat":13.4315},{"datetime":"2016-08-27 09:12:00","depth":10,"lng":60.9897,"src":"us","eqid":"us20006ui0","magnitude":4.8,"lat":-29.0991},{"datetime":"2016-08-27 01:28:10","depth":31.67,"lng":77.7042,"src":"us","eqid":"us20006ufk","magnitude":4.6,"lat":31.5284},{"datetime":"2016-08-26 22:17:30","depth":89.47,"lng":-69.6267,"src":"us","eqid":"us20006uea","magnitude":4.5,"lat":19.1586},{"datetime":"2016-08-27 08:40:30","depth":39.28,"lng":-166.7081,"src":"us","eqid":"us20006uhu","magnitude":4.5,"lat":53.1043},{"datetime":"2016-08-27 05:24:30","depth":448.57,"lng":146.819,"src":"us","eqid":"us20006ugu","magnitude":4.3,"lat":48.106},{"datetime":"2016-08-27 09:35:00","depth":73,"lng":-64.4466,"src":"pr","eqid":"pr16240006","magnitude":4.3,"lat":19.1912},{"datetime":"2016-08-27 04:46:30","depth":157.4,"lng":128.588,"src":"us","eqid":"us20006ugd","magnitude":4.3,"lat":-7.3908}]}'
                , jsonResponse
        );

    }

    @isTest
    static void testCalloutException()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestGeonamesJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);

        EarthquakeAppSettings__c settings = new EarthquakeAppSettings__c(Name='default', Geonames_Username__c='testUser');
        insert settings;

        test.startTest();

        Account acc = new Account(Name='Test Account');
        insert acc;

        try
        {
            String jsonResponse = GeonamesService.queryEarthquakesByBoundingBox(
                    new List<String>{'-85','85','-180','180'}, 10
            );
            system.assert(FALSE, 'Exception was expected as we are mixing DML and a callout');
        }
        catch(CalloutException ex)
        {
            system.assert(TRUE);
            system.assertEquals('You have uncommitted work pending. Please commit or rollback before calling out', ex.getMessage());
        }

        test.stopTest();
    }

    @isTest
    static void testMissingSettingException()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TestGeonamesJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);

        test.startTest();

        try
        {
            String jsonResponse = GeonamesService.queryEarthquakesByBoundingBox(
                    new List<String>{'-85','85','-180','180'}, 10
            );
            system.assert(FALSE, 'Exception was expected as we did not insert our custom settings.');
        }
        catch(ApplicationSettingException ex)
        {
            system.assert(TRUE);
            system.assertEquals('The EarthquakeApp settings have not been set. Please update the custom setting and try again.', ex.getMessage());
        }

        test.stopTest();
    }
}