/**
 * Created by tmowbrey on 8/23/2016.
 */

public with sharing class NominatimService
{
    public static String querySearchJSON(List<String> locations)
    {
        return service().querySearchJSON(locations);
    }

    private static NominatimServiceImpl service()
    {
        return new NominatimServiceImpl();
    }
}