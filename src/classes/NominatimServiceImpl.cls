/**
 * Created by tmowbrey on 8/23/2016.
 */

public class NominatimServiceImpl
{
    public String querySearchJSON(List<String> locations)
    {
        /*
        *  To meet the requirements of this assigment, I am not bulkifying this process.
        *  Typically, I would create a custom object to hold the batched location data, create a starting record,
        *  pass that record and the locations to a batch class for processing. On completion of processing that
        *  data would be saved to the database.
        *
        *  The return of this class would have bene a custom wrapper class containing the id of the database record
        *  and the batch process id. It would be upto the client to poll for finish and continue.false
        *
        *  For this assignment, I am going to do the call out here for the one location string expected from
        *  the client and pass back the response
        * */

        String responseBody;

        if(locations.size() == 1)
        {
            // Get Single Location
            String location = locations[0];

            // Construct HTTP request
            Http http = new Http();
            HttpRequest req = new HttpRequest();

            // Set Header Details
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');

            // Set Method and Endpoint
            req.setMethod('GET');
            req.setEndpoint('https://nominatim.openstreetmap.org/search?q='
                    + EncodingUtil.urlEncode(location, 'UTF-8') + '&format=json&limit=1');

            try
            {
                HttpResponse res = http.send(req);
                responseBody = res.getBody();

            }
            catch(System.CalloutException e)
            {
                throw e;
            }
        }

        return  responseBody;
    }
}