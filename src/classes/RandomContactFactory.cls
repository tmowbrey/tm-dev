public class RandomContactFactory 
{
	public static List<Contact> generateRandomContacts(Integer numOfContacts, String lastNameString)
    {
        List<Contact> contactList = new List<Contact>();
        
        for(Integer i = 0; i < numOfContacts; i++)
        {
            Contact con = new Contact(FirstName = 'Test ' + i, LastName = lastNameString);
            contactList.add(con);
        }
        
        return contactList;
    }
}