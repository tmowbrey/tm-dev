public class StringArrayTest 
{
	public static List<String> generateStringArray(Integer numberOfStringsToReturn)
    {
        List<String> returnList = new List<String>();
        
        for(Integer i = 0; i < numberOfStringsToReturn; i++)
        {
            String myString = 'Test ' + i;
            returnList.add(myString);
        }
        
        return returnList;
    }
}