@isTest
private class TestRestrictContactByName 
{
	@isTest
    private static void testInvalidName()
    {
        Contact con = new Contact(Firstname = 'Test', LastName = 'INVALIDNAME');
        
        try
        {
            insert con;
        }
        catch(Exception ex)
        {
            Boolean expectedExceptionThrown =  ex.getMessage().contains('The Last Name "'+con.LastName+'" is not allowed for DML') ? true : false;
            system.assert(expectedExceptionThrown);
        }
    }
}