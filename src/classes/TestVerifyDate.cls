@isTest
private class TestVerifyDate 
{
    @isTest
	private static void testCheckDate()
    {
        Date date1 = date.newInstance(2015, 01, 01);
        Date date2 = date.newInstance(2015, 01, 30);
        Date check1 = VerifyDate.CheckDates(date1, date2);
        
        system.assertEquals(date2, check1);
        
        date2 = date2 + 10; // Feb 9, 2015
        
        Date check2 = VerifyDate.CheckDates(date1, date2);
        
        system.assertEquals(date.newInstance(2015, 01, 31), check2);
    }
}