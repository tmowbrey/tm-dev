trigger AccountAddressTrigger on Account (before insert, before update, before delete, after insert, after update, after delete) 
{
	if(trigger.isBefore && !trigger.isDelete)
    {
        for(Account acc : trigger.new)
        {
            if(acc.Match_Billing_Address__c && acc.BillingPostalCode != NULL)
            {
                acc.ShippingPostalCode = acc.BillingPostalCode;
            }
        }
    }
}