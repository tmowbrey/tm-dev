trigger ClosedOpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete) 
{
	if(trigger.isAfter && !trigger.isDelete)
    {
        List<Task> tasksForInsert = new List<Task>();
        for(Opportunity opp : trigger.new)
        {
            if(opp.StageName == 'Closed Won')
            {
                Task t = new Task();
                t.WhatId = opp.Id;
                t.ActivityDate = Date.today();
                t.Subject = 'Follow Up Test Task';
                tasksForInsert.add(t);
            }
        }
        
        if(tasksForInsert.size() > 0)
            insert tasksForInsert;
    }
}